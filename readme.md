## Démo CI gitlab - Compilation/link/test d'un code de calcul

Nous allons maintenant considérer un exemple plus complet où l'intégration continue sera utilisée pour

* configurer et compiler du code c++ (et donc vérifier que ces étapes fonctionnent)
* tester l'exécution d'un code
* générer de la doc pour ce code
* publier cette doc

Dans ce projet, le répertoire [ci-tests](./ci-tests) contient du code c++, des fichiers de conf et de quoi lancer des tests.

Inutile de vous attarder sur le contenu précis de ces fichiers. Ce qu'il faut retenir est que pour pouvoir utiliser ce code, il faut :

* une étape de configuration (via CMake) dont le rôle est de vérifier la présence d'un compilateur, de dépendances du code etc
* une étape de compilation/édition de lien pour créer un exécutable et des tests (make)
* une étape de test (make test)


L'objectif de la démo sera donc de :

* mettre en place de l’intégration continue dans ce projet gitlab,
* gérer l'exécution automatique de la compilation du code et l'exécution des tests à chaque push.
* tester le code sur différents systèmes (ubuntu et debian pour l'exemple)
* utiliser nos propres images docker et profiter des 'registries' de gitlab


#### création des pipeline

On commence par créer un pipeline 'ubuntu' et un pipeline 'debian', chacun avec 3 jobs :

* un job configure qui exécute un script configure_soft.sh, dans un répertoire temporaire build, à conserver pour l'étape suivante

```
ubuntu:configure:
  stage: configure
  image: ubuntu:18.04
  # Liste des commandes à exécuter
  script:
    - sh ci-tests/ci/configure_soft.sh
  artifacts:
      paths:
        - build/
      expire_in: 2 days 
```

* un job build, qui reprend le résultat du configure et lance la compilation

```
ubuntu:build:
  stage: build
  image: ubuntu:18.04
   # Liste des commandes à exécuter
  script:
    - cd $CI_PROJECT_DIR/build
    - make -j 2
  needs: ["ubuntu:configure"]
```

* un job test, qui reprend les résultats de la compilation/édition de lien et exécute des tests

```
ubuntu:test:
  stage: test
  image: $CI_REGISTRY_IMAGE/mon-ubuntu
   # Liste des commandes à exécuter
  script:
    - cd $CI_PROJECT_DIR/build
    - make test
  needs: ["ubuntu:build"]
```

Notez l'utilisation du mot clé 'needs' pour introduire des dépendances et s'assurer qu'un job ne démarrera pas avant que ses prédecesseurs dans le pipeline ne soient terminés avec succès.

Les stages possibles sont définis en début de fichier, via le mot-clé stage.

Voici la première version complète du fichier .gitlab-ci.yml que nous allons tester et commenter dans la démo :

```
stages:
    - configure
    - build
    - test

before_script:
  - apt update -qq && apt install -y -qq cmake libboost-dev 
  - apt install -y -qq libfftw3-dev gfortran g++

workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
    - if: $CI_COMMIT_MESSAGE =~ /^\[all-jobs\]/
      when: always
    - when: never

ubuntu:configure:
  stage: configure
  image: ubuntu:18.04
  # Liste des commandes à exécuter
  script:
    - sh ci-tests/ci/configure_soft.sh
  artifacts:
      paths:
        - build/
      expire_in: 2 days 

ubuntu:build:
  stage: build
  image: ubuntu:18.04
   # Liste des commandes à exécuter
  script:
    - cd $CI_PROJECT_DIR/build
    - make -j 2
    - make test
  needs: ["ubuntu:configure"]
  artifacts:
      paths:
        - build/
      expire_in: 2 days 

ubuntu:test:
  stage: test
  image: ubuntu:18.04
   # Liste des commandes à exécuter
  script:
    - cd $CI_PROJECT_DIR/build
    - make test
  needs: ["ubuntu:build"]

debian:configure:
  stage: configure
  image: debian:buster
  # Liste des commandes à exécuter
  script:
    - sh ci-tests/ci/configure_soft.sh
  artifacts:
      paths:
        - build/
      expire_in: 2 days 
 
debian:build:
  stage: build
  image: debian:buster
  before_script:
  - apt update -qq && apt install -y -qq cmake libboost-dev 
  - apt install -y -qq libfftw3-dev gfortran g++
   # Liste des commandes à exécuter
  script:
    - cd $CI_PROJECT_DIR/build
    - make -j 2
  needs: ["debian:configure"]
  when: manual
  artifacts:
      paths:
        - build/
      expire_in: 2 days 

debian:test:
  stage: test
  image: debian:buster
  before_script:
  - apt update -qq && apt install -y -qq cmake libboost-dev 
  - apt install -y -qq libfftw3-dev gfortran g++
   # Liste des commandes à exécuter
  script:
    - cd $CI_PROJECT_DIR/build
    - make test
  needs: ["debian:build"]
  when: manual
  
  

```

#### Commentaires

* Installation de nouveaux packages pour chaque job

```
before_script:
  - apt update -qq && apt install -y -qq cmake libboost-dev 
  - apt install -y -qq libfftw3-dev gfortran g++
```

Ces lignes précisent que pour chaque image docker, il faut installer avant chaque job tout les packages listés ci-dessus.
Il est également possible d'utiliser le champ before_script pour chaque job.

* Le mot-clé 'needs' défini une dépendance entre des jobs.

* le mot clé 'when' permet de contrôler l'exécution du job. Dans l'exemple ci-dessus, 'manual' spécifie
que le job devra être déclenché manuellement via l'interface.
 
### Utilisation de gitlab registry et création automatique d'images docker

Dans l'exemple ci-dessus, à chaque push, les images docker sont chargées, démarrées et mise à jour (apt update et cie). Ceci n'est pas forcément utile et prend du temps et des ressources, alors que l'objectif est avant tout de tester le code, pas l'installation de package ubuntu.

Nous allons voir qu'il est possible d'utiliser l'intégration continue pour construire une image docker, la sauvegarder dans le projet puis
l'utiliser comme image pour les jobs suivants.

Trois étapes :

1. Activer les registries dans le projet
2. Ecrire un Dockerfile
2. Créer un job qui va générer une image docker à partir du Dockerile et la sauvegarder dans les registries


##### Activation des registries

Projet->Settings->General->Visibility, project features, permissions, cocher 'Container Registries'

##### Dockerfile

Créez un fichier Dockerfile dans le répertoire ci-tests/dockerfiles/ubuntu20.04 de votre projet : 

```
FROM ubuntu:20.04
RUN apt update  && apt install -y -qq \
    cmake \
    make \
    libboost-dev \
    gcc \
    gfortran \
    g++ \
    libfftw-dev
RUN apt autoclean -y && apt autoremove -y && rm -rf /var/lib/apt/lists/*
```

Ce fichier permet l'installation de divers packages (boost, cmake, ...) sur une ubuntu20.
La dernière ligne permet de supprimer ce qui n'est plus utile et d'éviter une image trop volumineuse.

##### Job CI 

Ajouter le job suivant dans votre .gitlab-ci.yml

```
ubuntu20:docker-build:
  # Created if the commit message starts with [docker-build] or
  # when the master branch is updated.
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    GIT_STRATEGY: clone
  stage: docker-build
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile ci_gitlab/dockerfiles/ubuntu20.04/Dockerfile --destination $CI_REGISTRY_IMAGE/mon-ubuntu:latest
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /^\[docker-build\]/
      when: always
    - when: never


```

Inutile d'entrer dans les détails de ce jobs. Il faut simplement retenir que :

* nous utilisons l'outil kaniko pour construire et pousser une image dans le registry du projet. Voir [gitlab et kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)
* il faut préciser le chemin (relatif) vers le dockerfile, après l'option '--dockerfile'
* il faut préciser le nom de l'image à créer après l'option '--destination'
* nous avons ajouter une règle précisant que ce job ne doit être exécuté que si le message commence par [docker-build]

Le reste est du copié-collé de la doc !

Ensuite, il sera possible d'utiliser cette image pour certains jobs ce qui évitera l'appel à "apt install" à chaque exécution.

Voici le [fichier .gitlab-ci.yml complet](./ci-tests/gitlab-ci-kaniko.yml)

##### Exécution ...

Après avoir modifié vos fichiers, commitez en commençant votre message par [docker-build] et vérifier le pipeline de CI.

Si le job se termine, vous devriez retrouver votre image docker dans les registries du projet et pouvoir vérifier que les jobs 'ubuntu'
ont bien tourné sur cette image.



![gitlab_registries.jpeg](images/gitlab_registries.jpeg)


Ensuite, nous pourrons utiliser cette image pour certains jobs.

Via les jobs d'intégration continue ci-dessus, nous avons :

- créé et sauvegardé des images docker,
- compilé le code sur plusieurs OS,
- généré de la doc pour le code et mis en ligne une page web avec la doc du code, grâce à un dernier job pages que vous trouverez en fin 
de fichier.



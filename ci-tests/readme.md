[[_TOC_]]

### Création d'un premier job d'intégration continue

Ajoutez à votre projet un fichier .gitlab-ci.yml contenant les lignes suivantes

```
# Construction du soft sur ubuntu:18.04
ubuntu:configure:
  image: ubuntu:18.04
  # Liste des commandes à exécuter
  script:
    - sh ci-tests/ci/configure_soft.sh
```

:information_source: si vous ajoutez ce fichier via l'interface gitlab (ou le WebIDE) vous trouverez des templates de fichier .gitlab-ci.yml


L'ajout de ce fichier dans le repository va entrainer :

* l'activation de l'intégration continue
* le déclenchement d'un job "ubuntu:configure" qui va

    * démarrer une image 'ubuntu18.04' sur un runner
    * exécuter les commandes listées après 'script' sur cette machine 

#### Vocabulaire

* **Pipeline** : une série de jobs exécutés sur un ou plusieurs runners.
  Chaque pipeline correspond à un commit et vous retrouvez dans le pipeline les jobs définis dans votre script (build:ubuntu dans votre cas).

* **job** : une suite d’actions exécutées sur un runner.
  Chaque job est indépendant. Rien n’est conservé à la fin du job (sauf demande explicite, voir plus loin).

* **runner** : une machine hôte qui va exécuter votre job.

#### Quelques commentaires

* job => démarrage d’une machine virtuelle basée sur l’image nommée dans le script. Nous reviendrons plus loin sur comment créer ou trouver ces images.
* votre projet est automatiquement cloné sur la nouvelle machine. Vos commandes seront exécutées dans le répertoire cloné.
Notez l'utilisation de la variable CI_PROJECT_DIR qui contient le chemin vers le repository cloné sur le runner.

* De nombreuses variables d'environnement sont définis par défaut par gitlab-ci et utilisables dans vos scripts, voir [GitLab CI/CD environment variables](https://docs.gitlab.com/ee/ci/variables/).

* Attention à la syntaxe. Vous pouvez vérifier votre .gitlab-ci.yml via le lien CI Lint de l’onglet CI/CD.

#### Résultats

Pour vérifier l'impact de l'ajout du fichier .gitlab-ci.yml, visitez le menu CI/CD->pipelines de votre projet.

Dans le menu CI/CD->pipelines, si vous cliquez sur un pipenline puis sur un job, vous obtiendrez un accès direct à la console.

![gitlab-ci-jobs.jpeg](gitlab-ci-jobs.jpeg)

Normalement, vous devriez constater que le pipeline a échoué : il manque cmake (et probablement d’autres dépendances).
Nous pouvons gérer l’installation de packages sur la machine virtuelle dans le script yml, corrigez le :

```
 ubuntu:configure:
  image: ubuntu:18.04
  before_script:
  - apt update -qq && apt install -y -qq cmake libboost-dev 
  - apt install -y -qq libfftw3-dev gfortran g++
  # Liste des commandes à exécuter
  script:
    - sh ci-tests/ci/configure_soft.sh
```

Le job doit maintenant aller au bout.

### Création d'un pipeline complet
Nous allons maintenant compléter notre script, en ajoutant les étapes de compilation/édition de lien et de tests.

Il est important de noter que chaque job est autonome. A la fin du job, la machine virtuelle est détruite, avec tout ce qu’elle contient.

Nous avons besoin de créer un job ubuntu:build qui dépendra de ubuntu:configure et aura besoin de ce qui aura été généré par cmake.
De même pour le job ubuntu:test qui dépendra de ubuntu:configure ; il faut que le projet ait été construit pour pouvoir lancer les tests.

Trois nouveaux mots-clés sont nécessaires pour gérer cette dépendance : stage, artifacts et dependencies ou needs.

* **stage** : un “étage” du pipeline, pouvant contenir plusieurs job exécutés en parallèle.
  
  * Pour passer au stage suivant, tous les jobs doivent avoir réussi (sauf en cas d'utilisation du mot-clé **needs**).
  * Chaque job doit faire partie d'un stage.
  * Voir [gitlab-ci stages](https://docs.gitlab.com/ee/ci/yaml/#stage)

* **artifacts** : des répertoires à conserver et à transmettre d’un job à l’autre.

* **needs** ou **dependency** : description des relations de dépendances entre jobs.

On commence donc par définir globalement les différents stages du projet

```
stages:
    - configure
    - build
```

puis on ajoute des artifacts au job ubuntu:configure

```
 ubuntu:configure:
  image: ubuntu:18.04
  stage: configure
  before_script:
  - apt update -qq && apt install -y -qq cmake libboost-dev 
  - apt install -y -qq libfftw3-dev gfortran g++
  # Liste des commandes à exécuter
  script:
    - sh ci-tests/ci/configure_soft.sh
  artifacts:
      paths:
        - build/
      expire_in: 2 days 
```

Notez la présence du expire_in qui assure la destruction des artifacts au bout de 2 jours et évite d'encombrer le serveur inutilement.
Pour plus de détails à ce sujet, voir [Defining job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).

Ensuite, complétez votre fichier .gitlab-ci.yml avec un nouveau job :

```
ubuntu:build:
  image: ubuntu:18.04
  before_script:
  - apt update -qq && apt install -y -qq cmake libboost-dev libfftw3-dev
  - apt install -y -qq libfftw3-dev gfortran g++
  # Liste des commandes à exécuter
  script:
    - cd $CI_PROJECT_DIR/build
    - make -j 2
    - make test
  needs: ["ubuntu:configure"]
  artifacts:
    paths:
      - build/
    expire_in: 2 days 
```

Notez que nous sommes obligés de réinstaller les dépendances pour chaque job. Seuls les artifacts sont conservés.

Pour terminer nous ajoutons les mêmes jobs mais cette fois sur une debian et en mode 'manual'.
Ce qui signifie qu'ils devront être déclenchés manuellement via l'interface gitlab.
Pour cela on utilse le mots clé 'when' avec la valeur 'manual'.

Voici le fichier complet :

```
stages:
    - docker-build
    - configure
    - build

before_script:
  - apt update -qq && apt install -y -qq cmake libboost-dev 
  - apt install -y -qq libfftw3-dev gfortran g++


ubuntu:configure:
  stage: configure
  image: ubuntu:18.04
  # Liste des commandes à exécuter
  script:
    - sh ci-tests/ci/configure_soft.sh
  artifacts:
      paths:
        - build/
      expire_in: 2 days 

ubuntu:build:
  stage: build
  image: ubuntu:18.04
   # Liste des commandes à exécuter
  script:
    - cd $CI_PROJECT_DIR/build
    - make -j 2
    - make test
  needs: ["ubuntu:configure"]

debian:configure:
  stage: configure
  image: debian:buster
  # Liste des commandes à exécuter
  script:
    - sh ci-tests/ci/configure_soft.sh
  artifacts:
      paths:
        - build/
      expire_in: 2 days 
  when: manual

debian:build:
  stage: build
  image: debian:buster
   # Liste des commandes à exécuter
  script:
    - cd $CI_PROJECT_DIR/build
    - make -j 2
    - make test
  needs: ["debian:configure"]
  when: manual

```

Notez que pour chaque job, vous avez accès (lecture, download ...) aux artifacts via l'interface :

![gitlab-ci-artifacts.jpeg](gitlab-ci-artifacts.jpeg)


Vous disposez maintenant d'un moyen de tester votre code automatiquement, à chaque push, sur différents systèmes.

### Compléments

Il est possible de contrôler l'intégration continue via le contenu des messages de commit.

* Si le message commence par [skip CI], les jobs ne sont pas lancés. 
 
* Vous pouvez précisez explicement des règles de fonctionnement. Par exemple :


```
workflow:
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: always
    - if: $CI_COMMIT_MESSAGE =~ /^\[all-jobs\]/
      when: always
    - when: never
```
Dans ce cas, si la branche ciblée s'appelle 'master' ou si le message de commit commence par [all-jobs], 
la règle est de lancer la CI. Dans le cas contraire, rien ne se passe.

:warning: fixer des règles est une bonne habitude à prendre pour éviter de lancer tout le process au moindre changement dans le code.
Rappelez vous de "Je code: les bonnes pratiques en écoconception !


### Bonus : choix du runner

**Runner** : une machine, un hôte sur lequel vont être exécutées les tâches d’intégration continue.

Jusqu’à présent, nous ne nous sommes pas interessés à la machine hôte des simulation, le **runner**.
Nous avons utilisé un runner par défaut, fourni par gricad-gitlab.

Un runner par défaut (shared) est disponible pour tous les projets. 

:warning: à n’utiliser que pour les phases de mise en place et de test !

Vous avez la possibilité de définir vos propres runners et d'en ajouter autant que vous le souhaitez.

#### Comment ?

Nous allons le faire en utilisant une des machines disponibles pour ce TP. 

Dans un premier temps, il faut installer docker et gitlab-runner sur la machine hôte.
Voir par exemple [gitlab runner pour ubuntu/debian](https://docs.gitlab.com/runner/install/linux-repository.html)

Une fois l'installation terminée, il faut enregistrée le runner :

```
> gitlab-runner register
...
```
Les informations à fournir sont indiqués dans les settings de votre projet : Settings du projet → CI/CD → Runner settings.

Une fois la procédure achevée, vous verrez apparaitre le runner dans la liste et il sera disponible pour faire tourner vos jobs.

### Bonus : générer une image docker et la stocker dans les registries du groupe ou du projet

Il est possible d'utiliser l'intégration continue pour construire une image docker et la sauvegarder dans le projet.

Trois étapes :

1. Activer les registries dans le projet
2. Ecrire un Dockerfile
2. Créer un job qui va générer une image docker à partir du Dockerile et la sauvegarder dans les registries


##### Activation des registries

Projet->Settings->General->Visibility, project features, permissions, cocher 'Container Registries'

##### Dockerfile

Créez un fichier Dockerfile dans le répertoire ci-tests/dockerfiles/ubuntu20.04 de votre projet : 

```
FROM ubuntu:20.04
RUN apt update  && apt install -y -qq \
    cmake \
    make \
    libboost-dev \
    gcc \
    gfortran \
    g++ \
    libfftw-dev
RUN apt autoclean -y && apt autoremove -y && rm -rf /var/lib/apt/lists/*
```

Ce fichier permet l'installation de divers packages (boost, cmake, ...) sur une ubuntu20.
La dernière ligne permet de supprimer ce qui n'est plus utile et d'éviter une image trop volumineuse.

##### Job CI 

Ajouter le job suivant dans votre .gitlab-ci.yml

```
ubuntu20:docker-build:
  # Created if the commit message starts with [docker-build] or
  # when the master branch is updated.
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    GIT_STRATEGY: clone
  stage: docker-build
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile ci_gitlab/dockerfiles/ubuntu20.04/Dockerfile --destination $CI_REGISTRY_IMAGE/mon-ubuntu:latest
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /^\[docker-build\]/
      when: always
    - when: never


```

Inutile d'entrer dans les détails de ce jobs. Il faut simplement retenir que :

* nous utilisons l'outil kaniko pour construire et pousser une image dans le registry du projet. Voir [gitlab et kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)
* il faut préciser le chemin (relatif) vers le dockerfile, après l'option '--dockerfile'
* il faut préciser le nom de l'image à créer après l'option '--destination'
* nous avons ajouter une règle précisant que ce job ne doit être exécuté que si le message commence par [docker-build]

Le reste est du copié-collé de la doc !

Ensuite, il sera possible d'utiliser cette image pour certains jobs ce qui évitera l'appel à "apt install" à chaque exécution.

Voici le [fichier .gitlab-ci.yml complet](./gitlab-ci-kaniko.yml)

##### Exécution ...

Après avoir modifié vos fichiers, commitez en commençant votre message par [docker-build] et vérifier le pipeline de CI.

Si le job se termine, vous devriez retrouver votre image docker dans les registries du projet et pouvoir vérifier que les jobs 'ubuntu'
ont bien tourné sur cette image.



![gitlab_registries.jpeg](gitlab_registries.jpeg)


### Publication des pages web

Dernière étape : un job d'intégration continue qui génère la doc et la publie sur un site web.

Il suffit d'ajouter un job 'pages' à notre script précédent :


```
# pages:
pages:
  stage : test
  image: $CI_REGISTRY_IMAGE/mon-ubuntu
  script:
    - mkdir build-doc; cd build-doc
    - cmake -DWITH_DOCUMENTATION=ON ../ci-tests/
    - make doc
    - mv doc/build/html ../public
  artifacts:
    paths:
      - public
  only:
    - master
  ```

Après votre commit/push, vous devriez voir deux nouveaux jobs dans votre pipeline : pages et deploy. 

Et si tout fonctionne correctement, vous aurez accès à votre site web :

https://ced2021.gricad-pages.univ-grenoble-alpes.fr/outils-devel-tps

Vous pouvez retrouver l’adresse du site généré via l’onglet Settings/Pages de votre projet.







# debug
macro(display V)
  message(STATUS "${V} = ${${V}}")
endmacro(display V)

# Visite une liste de répertoires et collecte tous les fichiers 'headers'
# La liste obtenue est stockée dans la variable HEADERS_FILES
# Utilisation:
# set(src_dirs dir1 dir2)
# get_headers(src_dirs)
function(get_headers slist)
  set(HEADERS_FILES)
  foreach(_DIR IN LISTS slist)
    # recupère les fichiers *.h, *.hpp
    file(GLOB FILES_LIST ${_DIR}/*.h ${_DIR}/*.hpp)
    if(FILES_LIST) # si non vide
      list(APPEND HEADERS_FILES ${FILES_LIST})
    endif()
  endforeach()
  if(HEADERS_FILES)
    list(REMOVE_DUPLICATES HEADERS_FILES)
    set(HEADERS_FILES ${HEADERS_FILES} PARENT_SCOPE)
  endif()
endfunction() 

# Scans DIRS (list of directories) and returns a list of all files in those dirs
# matching standard extensions for C/CXX/Fortran sources files.
# 
#
# Usage:
#
# set(source_dirs  dir1 dir2)
# get_sources("${source_dirs}")
#
# Result : set (parent scope) SOURCES_FILES with all matching files
# in dir1, dir2 ...
#
# Remarks:
# - dir1, dir2 ... are relative to CMAKE_CURRENT_SOURCE_DIR
function(get_sources source_dirs)
  message("Recherche des fichiers sources ...")
  set(SRC_EXTS cpp;f90;cxx)
  foreach(DIR IN LISTS source_dirs)
    message("Scan dir ${DIR} ...")
    foreach(_EXT IN LISTS SRC_EXTS)
      # Pour chaque répertoire dans la liste <source_dirs>,
      # on récupère les fichiers dont l'extension est dans SRC_EXTS.
      # Cette liste de fichiers est sauvées dans FILES_LIST.
      file(GLOB FILES_LIST ${DIR}/*.${_EXT})
      # On ajoute ce résultat (FILES_LIST)
      # à la liste globale (pour tous les répertoires), LOCAL_SOURCES
      if(FILES_LIST)
        list(APPEND LOCAL_SOURCES ${FILES_LIST})
      endif()
    endforeach()
  endforeach()
  # Suppression d'éventuels doublons dans SOURCES_FILES
  if(LOCAL_SOURCES)
    list(REMOVE_DUPLICATES LOCAL_SOURCES)
  endif()

  # La portée des variables étant limitée à la fonction
  # courante, il faut forcer la mise à jour
  # dans le CMakeListst.txt appelant
  set(SOURCES_FILES ${LOCAL_SOURCES} PARENT_SCOPE)
endfunction()

function(varscope)
  message(" --- Function ---")
  message("var2 dans fonction : ${var2}")
  set(varlocale "var locale")
  message("var locale fonction : ${varlocale}")
  set(var1 "new var1")
  message("var1 modifiée dans fonction : ${var1}")
  message("varcache dans fonction : ${var2}")
  set(var2 "new var2")
  message("var cache écrasée dans fonction : ${var2}")
  
endfunction()
macro(m_varscope)
  message(" --- Macro ---")
  message("var2 dans fonction : ${var2}")
  set(varlocale "var locale")
  message("var locale fonction : ${varlocale}")
  set(var1 "new var1")
  message("var1 modifiée dans fonction : ${var1}")
  message("varcache dans fonction : ${var2}")
  set(var2 "new var2")
  message("var cache écrasée dans fonction : ${var2}")
  
endmacro()



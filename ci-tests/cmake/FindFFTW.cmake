#  Siconos is a program dedicated to modeling, simulation and control
# of non smooth dynamical systems.
#
# Copyright 2018 INRIA.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# --
# Find FFTW libraries and headers
#
# Usage :
# 
# find_package(FFTW REQUIRED)
# target_link_libraries(yourlib PRIVATE FFTW::FFTW)
#
# It will handles both linking and include directories for your target.
#
# This module sets the following variables:
#
# FFTW_FOUND - set to true if a library implementing the FFTW interface
#    is found
#  FFTW_LIBRARIES - uncached list of libraries (using full path name) to
#    link against to use FFTW
#  FFTW_INCLUDE_DIR - location of blas headers found by cmake

# Set FFTW_DIR=where fftw is installed if it's not in a "classic" place or if you want a specific version
#

include(FindPackageHandleStandardArgs)

# -- First try : use FFTW_DIR provided explicitely at cmake call
if(FFTW_DIR)
  find_path(FFTW_INCLUDE_DIR NAMES fftw3.h
    PATHS ${FFTW_DIR}
    PATH_SUFFIXES include
    )
endif()

# --- Second try : pkg-config
# 
find_package(PkgConfig)
pkg_check_modules(PKGC_FFTW fftw3 QUIET)
if(PKGC_FFTW_FOUND)
  set(FFTW_LIBRARIES "${PKGC_FFTW_LINK_LIBRARIES}")
endif()

find_path(FFTW_INCLUDE_DIR NAMES fftw3.h
  PATHS ${PKGC_FFTW_INCLUDE_DIRS})

# --- Last try : default behavior ...
find_path(FFTW_INCLUDE_DIR NAMES fftw3.h)


if(NOT FFTW_LIBRARIES)
  if(FFTW_DIR)
    find_library(FFTW_LIBRARIES NAMES fftw3
    PATHS ${FFTW_DIR} ENV LIBRARY_PATH ENV LD_LIBRARY_PATH
    PATH_SUFFIXES lib lib64 
    )
    
  endif()
  find_library(FFTW_LIBRARIES NAMES fftw3)

endif()

# -- Library setup --
find_package_handle_standard_args(FFTW
  REQUIRED_VARS FFTW_LIBRARIES FFTW_INCLUDE_DIR)

if(FFTW_FOUND)
  
  if(NOT TARGET FFTW::FFTW)
    add_library(FFTW::FFTW UNKNOWN IMPORTED)
    set_property(TARGET FFTW::FFTW PROPERTY INTERFACE_LINK_LIBRARIES ${FFTW_LIBRARIES})
    if(FFTW_INCLUDE_DIR)
      set_target_properties(FFTW::FFTW PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${FFTW_INCLUDE_DIR}")
    endif()
    if(EXISTS "${FFTW_LIBRARIES}")
      set_target_properties(FFTW::FFTW PROPERTIES
        IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
        IMPORTED_LOCATION "${FFTW_LIBRARIES}")
    endif()
  endif()
endif()
mark_as_advanced(FFTW_LIBRARIES FFTW_INCLUDE_DIR)





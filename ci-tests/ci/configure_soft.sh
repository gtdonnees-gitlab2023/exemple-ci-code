


mkdir $CI_PROJECT_DIR/build ; cd $CI_PROJECT_DIR/build
export CXX=g++ FC=gfortran
cmake -D CMAKE_BUILD_TYPE=Debug -DWITH_TESTS=ON -DWITH_BOOST=ON -DWITH_FFTW=ON $CI_PROJECT_DIR/ci-tests/
